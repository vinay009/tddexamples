import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Tddclass {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	/* function isEven(n) {}
	 * Accepts 1 integer value, n
	 * 
	 * R1. If n is even, return true
	 * R2. If n is odd, return false
	 * R3. If N < 1, then return false
	 * R4. N must be > 1
	 */
	
	// R1. If n is even, return true
	@Test
	public void test() {
		evennumbers x = new evennumbers();
		boolean actualOutput = x.isEven(2000);
		assertEquals(true,actualOutput);
	}
	
	// R2: If n is odd, return false
		@Test
	 	public void testOddNumber() {
			evennumbers x = new evennumbers();
	 		boolean actualOutput = x.isEven(3);
		}
		
		// R3. If N < 1, then return false
			@Test
			public void testNLessThan1() {
				evennumbers x = new evennumbers();
				boolean actualOutput = x.isEven(-2);
				System.out.println("Function output: " + x.isEven(0));
				assertEquals(false, actualOutput);
			}
			
			// R4. N must be > 1
				@Test
				public void testNGreaterThan1() {
					evennumbers x = new evennumbers();
					boolean actualOutput = x.isEven(1000);
					assertEquals(true, actualOutput);
				}
	}
